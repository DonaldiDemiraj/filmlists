@extends('master')
@section("content")
<div class="container">
   <div class="row">
        <div class="col-sm-6">
            <img class="detail-img" src="{{$film['gallery']}}" alt="">
        </div>
        <div class="col-sm-6">
            <a href ="../">Go Back</a>
            <h2><b>{{$film['name']}}</b></h2>
            <h3><b>Status : </b>{{$film['status']}}</h3>
            <h4><b>Details : </b>{{$film['description']}}</h4>
            <h4><b>Genres : </b> {{$film['genres']}}</h4>
            <br><br>
            <form action="add_to_cart" method="POST">
                @csrf
                <input type="hidden" name="film_id" value={{$film['id']}}>
                <button class="btn btn-primary">Add To Cart</button>
            </form>
            <br><br>
        </div>
   </div>
</div>
@endsection
