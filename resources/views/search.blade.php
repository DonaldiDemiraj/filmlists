@extends('master')
@section("content")
<div class="custom-product">
    <div class="clo-sm-4">
        <a href="#">Filter</a>
    </div>
    <div class="col-sm-4">
        <div class="trending-wrapper">
            <h2>Result for Films</h2>
            @foreach($films as $item)
                <div class="searched-item">
                    <a href="detail/{{$item['id']}}">
                        <img class="trending-image" src="{{$item['gallery']}}">
                        <br/><br/><br/>
                        <div class="">
                            <br/>
                            <h2 style="color: tomato">{{$item['name']}}</h2>
                            <h5 style="color: tomato">{{$item['description']}}</h5>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
