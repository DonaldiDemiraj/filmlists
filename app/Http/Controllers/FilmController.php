<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Cart;
use App\Models\Order;
use Session;
use Illuminate\Support\Facades\DB;


class FilmController extends Controller
{
    function index()
    {
        $data = Film::all();
        return View('film',['films'=>$data]);

    }

    function detail($id)
    {
        $data = Film::find($id);
        return view('detail',['film'=>$data]);
    }

    function search(Request $request)
    {
        $data = Film::
        where('name', 'like', '%'.$request->input('query').'%')->get();
        return View('search',['films'=>$data]);
    }

    function addToCart(Request $request)
    {
        if($request->session()->has('user'))
        {
            $cart = new Cart;
            $cart->user_id = $request->session()->get('user')['id'];
            $cart->film_id = $request->film_id;
            $cart->save(); 
            return redirect('./');   
        }
        else
        {
            return redirect('/login');
        }
    }

    static function cartItem()
    {
        $userId = Session::get('user')['id'];
        return Cart::where('user_id',$userId)->count();
    }

    function cartList()
    {
        $userId = Session::get('user')['id'];
        $films = DB::table('cart')
        ->join('films','cart.film_id','=','films.id')
        ->where('cart.user_id',$userId)
        ->select('films.*','films.id as film_id')
        ->get();

        return View('cartlist',['films'=>$films]);
    }

    function  removeCart($id)
    {
        Cart::destroy($id);
        return redirect('/cartlist');
    }

    function orderNow()
    {
        $userId = Session::get('user')['id'];
        $total = $films = DB::table('cart')
        ->join('films','cart.film_id','=','films.id')
        ->where('cart.user_id',$userId);

        return View('ordernow',['total'=>$total]);
    }

    function orderPlace(Request $req)
    {
        $userId = Session::get('user')['id'];
        $allCart = Cart::where('user_id',$userId)->get();
        foreach($allCart as $cart)
        {
            $order = new Order;
            $order->film_id = $cart['film_id'];
            $order->user_id = $cart['user_id'];
            $order->status = "pending";
            $order->payment_method = $req->payment;
            $order->payment_status = "pending";
            $order->address = $req->address;
            $order->save();
            Cart::where('user_id',$userId)->delete();
        }
        $req->input();
        return redirect('/');
    }

    function myOrders()
    {
        $userId = Session::get('user')['id'];
        $orders =  DB::table('orders')
        ->join('films','orders.film_id','=','films.id')
        ->where('orders.user_id',$userId)
        ->get();

        return View('myorders',['orders'=>$orders]);
    }

}
